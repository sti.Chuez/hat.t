package com.example.evotec.hat_trick;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityCrearCampeonato extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_crear_campeonato);
        txte1 = (EditText)findViewById(R.id.txte1);
        txte2 = (EditText)findViewById(R.id.txte2);
        txte3 = (EditText)findViewById(R.id.txte3);
        txte4 = (EditText)findViewById(R.id.txte4);
        txte5 = (EditText)findViewById(R.id.txte5);
        txte6 = (EditText)findViewById(R.id.txte6);

        btn_guardar = (Button)findViewById(R.id.btnGuardar);

        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new  Intent (ActivityCrearCampeonato.this, RecivirParametro.class);
                Bundle bundle = new Bundle();

                bundle.putString("dato1",txte1.getText().toString());
                bundle.putString("dato2",txte2.getText().toString());
                bundle.putString("dato3",txte3.getText().toString());
                bundle.putString("dato4",txte4.getText().toString());
                bundle.putString("dato5",txte5.getText().toString());
                bundle.putString("dato6",txte6.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });


    }

    EditText txte1, txte2, txte3, txte4, txte6, txte5;
    Button btn_guardar;

}



